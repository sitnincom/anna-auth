"use strict";

const async = require("asyncawait").async;
const await = require("asyncawait").await;

const Model = require("anna").Model;

module.exports = class Persona extends Model {
    constructor (app, config) {
        super(app, config);

        this.shouldBePreloaded = true;
    }

    get __dirname () {
        return __dirname;
    }

    get loadByIdQueryFile () {
        return "LoadPersonaById";
    }

    get loadAllQueryFile () {
        return "LoadAllPersonas";
    }

    bySurnameAsc (a, b) {
        return a.surname <= b.surname ? -1 : 1;
    }

    create (displayName, tags) {
        return async(() => {
            const query = this.sqlFile("CreatePersona");
            return await(this.db.oneOrNone(query, [displayName, JSON.stringify(tags)]));
        })();
    }

    addTag (persona_id, tagname) {
        return async(() => {
            const persona = await(this.getById(persona_id));

            if (!persona.tags) { persona.tags = []; }
            persona.tags.push(tagname);

            const query = this.sqlFile("UpdatePersonaTags");
            const op = await(this.db.query(query, [persona.id, JSON.stringify(persona.tags)]));
            if (!op) {
                throw new Error("Cannot update persona's taglist");
            }

            return persona;
        })();
    }

    removeTag (persona_id, tagname) {
        return async(() => {
            const persona = await(this.getById(persona_id));

            if (!persona.tags) { persona.tags = []; }
            persona.tags = persona.tags.filter(tag => {
                return tag != tagname;
            });

            const op = await(this.db.query(this.sqlFile("UpdatePersonaTags"), [persona.id, JSON.stringify(persona.tags)]));
            if (!op) {
                throw new Error("Cannot update persona's taglist");
            }

            return persona;
        })();
    }

    markLastSeen (persona_id) {
        return this.db.query("UPDATE public.personas SET last_seen_at=NOW() WHERE id=$1;", [persona_id]);
    }
};
