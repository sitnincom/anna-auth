"use strict";

const async = require("asyncawait").async;
const await = require("asyncawait").await;
const bcrypt = require("bcryptjs");

const Model = require("anna").Model;

module.exports = class AuthLocal extends Model {
    get __dirname () {
        return __dirname;
    }

    get loadByIdQueryFile () {
        return "LoadAuthByLogin";
    }

    bcryptComparePromise (test, hash) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(test, hash, (err, res) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(res);
                }
            });
        });
    }

    // should return persona reference id if login/password pair is correct and false otherwise
    isPasswordValidForLogin (login, password) {
        // TODO: Check input
        return async(() => {
            const auth = await(this.getById(login));
            //this.log.debug(`LOCAL AUTH MODEL ${login} ${inspect(auth)}`);

            if (!auth) {
                throw new Error(`Cannot find login authenticator for login [${login}]`);
            }

            const test_res = await(this.bcryptComparePromise(password, auth.passhash));
            return test_res ? auth.persona : false;
        })();
    }
};
