"use strict";

const async = require("asyncawait").async;
const await = require("asyncawait").await;

const Model = require("anna").Model;

module.exports = class AuthFacebook extends Model {
    get __dirname () {
        return __dirname;
    }

    get loadByIdQueryFile () {
        return "LoadAuthByFacebook";
    }

    createAuthLinkForPersona(data, persona) {
        return this.db.query(this.sqlFile("CreateFacebookAuthLink"), {
            id: data.profile.id,
            token: data.accessToken,
            persona: persona.id,
            profile: data.profile,
        });
    }

    updateAuthToken(accessToken, refreshToken) {
        return this.db.query(this.sqlFile("UpdateFacebookAuthToken"), [accessToken, refreshToken]);
    }
};
