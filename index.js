"use strict";


const inspect = require("util").inspect;
const bodyParser = require('body-parser');
const async = require("asyncawait").async;
const await = require("asyncawait").await;

const FacebookStrategy = require('passport-facebook').Strategy;
const LocalStrategy = require('passport-local').Strategy;

const WebAppModule = require("anna").WebAppModule;

const Persona = require("./lib/Persona");
const AuthLocal = require("./lib/AuthLocal");
const AuthFacebook = require("./lib/AuthFacebook");

module.exports = class AuthModule extends WebAppModule {
    constructor (app, config) {
        super(app, config);

        this.app.addNodeModule("passport");
    }

    get models () {
        return [Persona, AuthLocal, AuthFacebook];
    }

    get globalMiddlewares () {
        return [
            this.passport.initialize(),
            this.passport.session(),
        ];
    }

    get passport () {
        return this.app.getNodeModule("passport");
    }

    // Save user to session data
    serializeUser (user, done) {
        // всегда передаём как есть
        done(null, user);
    };

    // Load user from session data
    deserializeUser (user, done) {
        // если user — целое, значит нужно загрузить, если объект — пользователь аутентифицирован,
        // но не авторизован и данные нужно передать как есть.

        if (isNaN(user)) {
            done(null, user);
        } else {
            this.log.debug(`Loading persona from session ${user}`);

            const p = this.app.getModel('Persona');
            p.getById(user).then(persona => {
                done(null, persona);
            });
        }
    }

    updateResLocals (req, res) {
        const user_menu = this.app.config["user_menu"];

        if (!user_menu) return;

        let eff_user_menu = [];

        if (!req.user && user_menu.hasOwnProperty("guest")) {
            eff_user_menu = eff_user_menu.concat(user_menu.guest);
        }

        if (req.user && req.user.id && req.user.tags.indexOf("admin") > -1) {
            eff_user_menu = eff_user_menu.concat(user_menu.admin);
        }

        if (req.user && req.user.id) {
            eff_user_menu = eff_user_menu.concat(user_menu.user);
        }

        res.locals.ui.user_menu = eff_user_menu;
    }

    init () {
        this.log.debug(`${this.constructor.name} init()`);

        this.passport.serializeUser(this.serializeUser.bind(this));
        this.passport.deserializeUser(this.deserializeUser.bind(this));

        // --- Logout handler
        this.router.get('/auth/logout', (req, res) => {
            req.logout();
            res.redirect('/');
        });

        // --- Login/Password auth
        this.passport.use(new LocalStrategy({
            usernameField: 'login',
            passwordField: 'password',
        }, (login, password, done) => {
            this.log.debug("LOCAL AUTH", login, password);
            const al = this.app.getModel('AuthLocal');
            al.isPasswordValidForLogin(login, password).then(persona_id => {
                this.log.debug("LOCAL AUTH SUCC", login, persona_id);
                done(null, persona_id);
            }).catch(err => {
                this.log.warn("LOCAL AUTH FAIL", login);
                done(err);
            });
        }));

        this.router.get('/auth/login/', (req, res) => {
            this.defaultTheme.render("auth/login.njk", res);
        });

        this.router.post('/auth/login/', [bodyParser.urlencoded({ extended: true })], this.passport.authenticate('local', {
            session: true,
            successRedirect: "/auth/success",
            failureRedirect: '/auth/login/',
        }));

        // --- Success auth handler
        this.router.get('/auth/success', (req, res) => {
            this.log.debug("AUTH SUCC", JSON.stringify(req.user));
            if (!req.user.hasOwnProperty("id")) {
                throw new Error("Cannot be called directly");
            } else {
                const p = this.app.getModel("Persona");
                p.markLastSeen(req.user.id);
                this.log.debug("AUTH SUCC -> MY");
                res.redirect("/");
            }
        });

        // --- Facebook social auth

        this.passport.use(new FacebookStrategy({
            clientID: this.app.config.auth.facebook.app_id,
            clientSecret: this.app.config.auth.facebook.app_secret,
            callbackURL: `https://${this.app.config.site.domain}/auth/fb/callback`,
        }, (accessToken, refreshToken, profile, done) => {
            // Find or create user
            this.log.debug("FB AUTH", inspect(accessToken), "|", inspect(refreshToken), "|", inspect(profile));

            const p = this.app.getModel("Persona");
            const afb = this.app.getModel("AuthFacebook");

            async(_ => {
                const auth = await(afb.getById(profile.id));
                if (!auth) {
                    const persona = await(p.create(profile.displayName, ["user", "unmanaged"]));
                    if (!persona || !persona.id) {
                        done(new Error("Cannot create new unmanaged Persona"));
                    } else {
                        const link = await(afb.createAuthLinkForPersona({
                            //provider: "fb",
                            accessToken: accessToken,
                            refreshToken: refreshToken,
                            profile: profile,
                        }, persona));
                        if (!link) {
                            done(new Error("Cannot create Facebook to Persona link"));
                        } else {
                            done(null, persona.id);
                        }
                    }
                } else {
                        const persona = await(p.getById(auth.persona));
                    if (!persona) {
                        done(new Error("Не удалось загрузить данные учётной записи по токену Facebook"));
                    }

                    if (refreshToken) {
                        await(afb.updateAuthToken(profile.id, accessToken, refreshToken));
                    }

                    done(null, persona.id);
                }
            })();
        }));

        this.router.get('/auth/fb/', this.passport.authenticate('facebook', { scope: this.app.config.auth.facebook.scope }));
        this.router.get('/auth/fb/callback', this.passport.authenticate('facebook', {
            successRedirect: '/auth/success',
            failureRedirect: '/',
        }));

    }
};
